
const { Int32 } = require("mongodb");
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let emailsignup = new Schema(
    {
        Name: {
            type: String
        },
        password: {
            type: String
        },
        phonenumber: {
            type: String
        }

    },
    { collection: "Customerdetails" }
);

module.exports = mongoose.model("EmailDetails", emailsignup);
